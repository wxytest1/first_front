#!/bin/bash

set -e

ssh root@115.28.168.65 /bin/bash << EOF
cd /opt/works/first_front
git co .
git pr
killall -9 uwsgi
python manage.py collectstatic<<!
yes
!
EOF