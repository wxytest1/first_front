from django.contrib import admin

from .models import Article
from .models import Person


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'content')

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


class PersonAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name')
    search_fields = ('last_name',)

    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super(PersonAdmin, self).get_search_results(request, queryset, search_term)
        try:
            queryset |= self.model.objects.filter(last_name=search_term)
        except:
            pass
        return queryset, use_distinct


admin.site.register(Article, ArticleAdmin)
admin.site.register(Person, PersonAdmin)

# Register your models here.
